provider "google" {
    credentials = "${file("seeed-labs-bb75e72c1cb7.json")}"
    project = "seeed-labs"
    region = "us-central1"
    zone = "us-central1-c"


}
resource "google_compute_instance" "VM-instance" {
    name        ="instance-3"
    machine_type ="f1-micro"
    zone        ="us-central1-a"

    tags = ["db"]

    boot_disk {
        initialize_params {
            image = "centos-8-v20200316"
        }

    }


    network_interface {
        network = "${google_compute_network.vpc_network.self_link}"
        access_config {

        }
    }


    
}

resource "google_compute_network" "vpc_network" {
    name                    = "terraform-network"
    auto_create_subnetworks = "true"
}